# Exploring Tribalism - A playable post on tribalism

In Kenya, we have over 40 tribes. We are culturally diverse. Yet, at every election period, or any major political event for that matter, our culturally diversity works against us- it separates us.

This is a playable post that seeks to educate the general public how divisive our perceptions on tribes is. This post has been adapted from Ncase's work on Polygons which you can find [here](ncase.me/polygons/).

# Part 1: Introduction

For the sake of simplicity, let's create a fictional country with fictional tribes. Our fictional country, Kôlowezi, has 2 major tribes: Ariwaya and Logomelo(I made up those names after looking at [this](https://en.wikipedia.org/wiki/List_of_fictional_African_countries) list).

```image
// image of the 2 tribes
```
Our country, Kôlowezi, comprises 50% Ariwaya people and 50% Logomelo people; and everyone is *slightly* tribalist. These people console themselves that it is their pride in their culture that only drives them to be "slightly" tribal, in fact, everyone prefers to be in a diverse crowd. They preach love, peace and unity where they go, but deep inside, they are "slightly" tribalistic.

# Part 2: Tribal Clusters
Kôlowezians will only want to move if they are unhappy with their immediate neighborhood. Once a Kôlowezian is happy where they are, **you can't move them until they're unhappy with their neighbors again**. In their heads, the Kôlowezians have one simple rule:

"I wanna move if less than 1/3 of my neighbors are like me."

```javascript
canvas 2 here
```

Harmless, right? Every Kôlowezian would be happy with a mixed neighborhood. Surely their small bias about tribe can't affect the larger shape of society, right? Well ...

```javascript
// drag and drop unhappy Kôlowezians until nobody is unhappy(just move them to random empty spots; don't think too much about it)
canvas 3 here
```


Goddamn, our society has become super segregated. Goddaaaamn!!!! "Kumbe utajua hujui".

Sometimes, a neighborhood just becomes an Ariwayan turf, and it's not their fault(Ariwayans) if no Logomelo folks wanna stick around. And a Logomelo neighborhood would become an Ariwayan neighborhood, but they can't help it if Ariwayans ain't interested.

```javascript
canvas 4 here
```

# Part 3: Segregation
In this next bit, unhappy people automatically move to random empty spots. There's also a graph that tracks how much segregation there is over time.

```javascript
// run this simulation a few times. What happens?
canvas 5 here
```

This has been a bit counter-intuitive. These people are good, decent, hard-working people; and yet, though every individual only has a slight bias, the entire society is split along very clear tribal lines. The implications of this is very scary.

Some leaders among the people may take advantage of this and divide the people even further in a bid to cement their power.

# Part 4: Collective Bias

```javascript
\\ Make us happy
canvas 6 here
```
*Small individual bias can lead to large collective bias.*

Equality is an unstable equilibrium. The smallest of bias can push a whole society past the tipping point. Well, what if we taught our people to have zero bias?(or maybe more bias, that's upto you):

```javascript
// use the slider to adjust the shapes' individual bias:
canvas 7 here
```

Notice how much more segregated things become, when you increase the bias beyond 33%. What if the threshold was at 50%? Seems reasonable for someone in our country to prefer not being in the minority...

```javascript
// shapes that want to move
```

So yeah, just turn everyone's bias to 0, right? Haha, Well; things in the real world are not that dandy. The real world doesn't start a new with a random shuffling of citizens every day. Everyday, you're not shuffling.

# Part 5: A segregated world
Let's assume the world starts segregated. What happens when you lower the bias?

```javascript
//another canvas
```

See what doesn't happen? No change. No mixing back together. In a world where bias never existed, being unbiased isn't enough! We're gonna need active measures. What if people wanted people wanted to seek more variety? Perhaps this is the so called rise of millenials who seek tribalism.

```javascript
wants to move if more than 90% of their neighbours are like them
```

Woah. Even though each person would be okay with having up to 90% of their neighbors that are like them, they all mix together! Let's see this play out on a larger scale, when we change the amount of bias and anti-bias for all people

# Part 6: Diversity

**world starts segregated. what happens when people demand even the smallest bit of diversity?**

All it takes is a change in the perception of what an acceptable environment looks like. So, fellow people, remember it's not about Logomelo vs Ariwaya, it's about deciding what we want the world to look like, and settling for no less.

# Part 7: Friendship
GET THEM ALL IN THE BOX OF F R I E N D S H I P
(hint: don't move them straight to the box; keep the pairs close together)
```javascript
yac[yet another canvas]
```

At first, going out on your own can be isolating... but by working together, step by step, we'll get there.

# Part 8: A sandbox

A sandbox to play around in:
```javascript
// sandbox
```

# Part 9: Wrapping up
Wrapping up
 1. Small individual bias → Large collective bias.
When someone says a culture is shapist, they're not saying the individuals in it are shapist. They're not attacking you personally.

2. The past haunts the present.
Your bedroom floor doesn't stop being dirty just coz you stopped dropping food all over the carpet. Creating equality is like staying clean: it takes work. And it's always a work in progress.

3. Demand diversity near you.
If small biases created the mess we're in, small anti-biases might fix it. Look around you. Your friends, your colleagues, that conference you're attending. If you're all triangles, you're missing out on some amazing squares in your life - that's unfair to everyone. Reach out, beyond your immediate neighbors.

# Part 10: Credits
