import Vue from 'vue';
import Router from 'vue-router';
import Introduction from '@/components/Introduction';
import Scene from '@/components/Scene';

// Scenes
import scene1 from '@/components/Scenes/1';
import scene2 from '@/components/Scenes/2';
import scene3 from '@/components/Scenes/3';
import scene4 from '@/components/Scenes/4';
import scene5 from '@/components/Scenes/5';
import scene6 from '@/components/Scenes/6';
import scene7 from '@/components/Scenes/7';
import scene8 from '@/components/Scenes/8';
import scene9 from '@/components/Scenes/9';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
    },
    {
      path: '/scene/',
      component: Scene,
      children: [
        {
          path: '',
          component: scene1,
        },
        {
          path: '1',
          component: scene1,
        },
        {
          path: '2',
          component: scene2,
        },
        {
          path: '3',
          component: scene3,
        },
        {
          path: '4',
          component: scene4,
        },
        {
          path: '5',
          component: scene5,
        },
        {
          path: '6',
          component: scene6,
        },
        {
          path: '7',
          component: scene7,
        },
        {
          path: '8',
          component: scene8,
        },
        {
          path: '9',
          component: scene9,
        },
      ],
    },
  ],
});
