import Vue from 'vue';
import Scene from '@/components/Introduction';

describe('Introduction.vue', () => {
  it('should render correnct contents', () => {
    const Constructor = Vue.extend(Scene);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('h4').textContent)
      .to.equal('A Playable Post on Tribalism');
  });
});
